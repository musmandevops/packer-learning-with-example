packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

locals {
  login-ssh-key = vault("kv/data/ssh-keys","public-key")
}

source "amazon-ebs" "amz-linux" {
  ami_name      = "neustreet-basic-web-server"
  tags = {
    name  = "neustreet-webserver"
    env   = "dev" 
  }
  instance_type = "t2.micro"
  region        = "us-east-1"
  source_ami_filter {
    filters = {
      name                = "amzn2-ami-hvm-2.0.*-gp2"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["amazon"]
  }
  ssh_username = "ec2-user"
}

build {
  sources = ["source.amazon-ebs.amz-linux"]
  provisioner "shell" {
    inline = [
      "mkdir -p ~/.ssh/"
      "echo ${local.login-ssh-key} > ~/.ssh/authorized_keys",
      "sudo yum -y install httpd && sudo systemctl start httpd",
      "sudo echo '<h1><center>My Test Website With Help From Packer and Terraform</center></h1>' > index.html",
      "sudo mv index.html /var/www/html/",
      "sudo systemctl enable httpd.service"
      ]
  }
  post-processor "manifest" {
        output = "manifest.json"
        strip_path = true
    }
  post-processor "shell-local" {
    scripts = ["get_ami_id_to_tf_vars.sh"]
  }
}