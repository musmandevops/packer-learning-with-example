cat <<EOT >> ami_id_var.tf
variable "ami_id"{
  default="$(jq -r '.builds[-1].artifact_id' manifest.json | cut -d ":" -f2)"
}
EOT