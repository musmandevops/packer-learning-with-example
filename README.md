1. create KV secrect in vault with name ssh-keys and add below ssh keys as secrets.
    1. public-key
    2. private-key

2. Install packer on your local machine if it's not already installed.

3. set vault credentilas as env variables.
    1. VAULT_ADDR
    2. VAULT_NAMESPACE
    3. VAULT_TOKEN
    4. AWS_ACCESS_KEY_ID
    5. AWS_SECRET_ACCESS_KEY

4. run command ```packer init . && packer build .``` command to build image.


5. Install Terraform on your local machine if it's not already installed.

6. run command ```terraform init && terraform apply``` 

7. terraform command will return public ip of ec2 instance.

8. Install Vault on your local machine if it's not already installed.

9. get ssh login key from vault run command ```vault kv get --field=private-key kv/ssh-keys > tmp-key && chmod 400 tmp-key```

10. To login on ec2 instance run command ```ssh -i tmp-key ec2-user@public-ip```.
